# linear model
#first we start with data entry
library(readxl)
Data_Hamedan <- read_excel("file address")
view(Data_Hamedan)
mydata <- Data_Hamedan
#you can select a random sample from data if you want
set.seed(1234)
#This is a wrapper around sample.int() to make it easy to select random rows from a table. It currently only works for local tbls.
dplyr::sample_n(mydata, 10)
###normality check for outcome variable as an assumption fer the linear model
library(ggpubr)
ggppplot(mydata$CSP)
Shapiro.test(mydata$csp)
####running a linear rgression model
library(ggplot2)
library(dplyr)
library(readr)
library(knitr)
library(broom)
CSP_model <- lm(CSP~Year, data=mydata)
view(CSP_model)
summary(CSP_model)
###some treatments for residuals
Shapiro.test(residuals(CSP_model))
ggqqplot(residuals(CSP_model))
#Residuals vs fitted values
plot(CSP_model, 1)
#Q-Q plot for normality check of residuals
plot(CSP_model, 2)
#plot for constancy of variance and independency of residuals
plot(CSP_model, 3)
###now for confirmation of outocorrelation in residuals
library(car)
durbinwatsonTest(CSP_model)
#you can do it with add some lags
durbinwatsonTest(CSP_model, max.lag=3)
#Since this p-value is less than 0.05, we can reject the null hypothesis and conclude that the residuals in this regression model are autocorrelated.